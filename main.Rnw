\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=0.7in]{geometry}
\usepackage{tikz}
\usepackage{indentfirst}
\usepackage{chemmacros}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{achemso}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage[section]{placeins}
\usepackage{todonotes}
\usepackage[nolist]{acronym}
\usepackage{varioref}
\usepackage[capitalise,noabbrev]{cleveref}

\newacro{MLRA}{Multi-wavelength Linear Regression Analysis}
\newacro{SSR}{sum of squared residuals}

\usechemmodule{units,redox}

\renewcommand*{\thefootnote}{\alph{footnote}} %letters instead of numbers for footnotes

\sisetup{
  round-mode      = figures,
  round-precision = 3,
  round-half      = even,
  table-auto-round,
  multi-part-units= single,
  list-units      = single
}%

\captionsetup[figure]{width=0.8\linewidth}
\captionsetup[table]{width=0.8\linewidth}

<<setup, include=FALSE, cache=FALSE>>=
## innitialization ------
	library('knitr')
	opts_chunk$set(cache=FALSE, echo=FALSE, message=FALSE,warning = FALSE, fig.height = 4, fig.width = 6)

	if (!require("pacman")) install.packages("pacman")
	pacman::p_load("data.table", "ggrepel", "ggpmisc", "magrittr", "ggplot2", "xtable", "tikzDevice")
	
  options(digits = 6, tikzDefaultEngine = 'pdftex')
	source("./Scripts/UtilityScripts.R")
	options(xtable.include.rownames = FALSE,
		xtable.caption.placement = "top",
		xtable.booktabs = TRUE,
		xtable.sanitize.text.function = function (x) {x},
		xtable.table.placement = "hb!")
## parameters -------
  C_CO_STD <- 0.1880 #mol/L
  C_CR_STD <- 0.0750 #mol/L
	MIX_NO <- 14L
## calculations ----------
	Absorbances <- fread("./Data/Data.csv",header = T,encoding = "UTF-8") %>% setnames(1,'Wavelength')
	Absorbances[
	  ,c("Co","Cr","Mix","Blank") := .(Co - Blank,Cr -Blank, Mix -Blank,NULL)][
	  ,c("el_co","el_cr") := .(Co/C_CO_STD,Cr/C_CR_STD)][
	  ,c("Mix_Co","Cr_Co"):= .(Mix/Co,Cr/Co)]
	
	glm_conc <- with(Absorbances, glm(Mix~el_co+el_cr+0))
  lm_mlra <-  with(Absorbances, lm(Mix_Co~Cr_Co))
  
	res_mlra<- data.table(summary(lm_mlra)$coefficients, keep.rownames = T)[
    ,.(Species = c("Co","Cr"),
       Concentration = Estimate*c(C_CO_STD,C_CR_STD),
       Err = `Std. Error` * c(C_CO_STD,C_CR_STD))] %>% setkey(Species)
	
	Absorbances[,Solver := predict(glm_conc,.SD)][
	            ,MLRA := el_co*res_mlra["Co",Concentration] +
	                     el_cr*res_mlra["Cr",Concentration] ]
	Errs <- Absorbances[,.(
    Wavelength,
    Solver = (Solver - Mix) / Mix * 100,
    MLRA = (MLRA - Mix) / Mix * 100
  )]

## plots -----------
	wlplot <-ggplot(
  	  Absorbances[,.(Wavelength,Co,Cr,Mix,Solver,MLRA)] %>% 
  	    melt.data.table(id.vars = "Wavelength",value.name = "Absorbance",variable.name = "Legend")
  	 ,aes(x=Wavelength, y= Absorbance, color=Legend)) +
	  geom_line(aes(linetype=Legend)) +
	  geom_point(size=2L, aes(shape=Legend))+
	  scale_shape_manual(values = c(0,0,0,32,32)) +
	  scale_linetype_manual(values=c(1,1,1,2,4)) +
	  xlab("Wavelength (\\si{\\nano\\meter})") +
	  theme_classic()

	errplot <- ggplot(melt.data.table(Errs,id.vars = "Wavelength",
                                   variable.name = "Model",
                                   value.name = "Difference")
                   ,aes(Wavelength, Difference ,fill = Model)) +
    geom_col(position = 'dodge') +
    geom_hline(yintercept = 0) +
    theme_classic() + labs(x = "Wavelength (\\si{\\nano\\meter})", y = "{Difference From Measurement (\\%)}")


  mlraplot <-ggplot(
    Absorbances[,.(Wavelength,Mix_Co,Cr_Co)],
    aes(Cr_Co,Mix_Co)) +
      stat_smooth(method = 'lm',se = F, color = 'gray', linetype = 'dashed') +
      geom_point() +
      stat_poly_eq(aes(label=paste("\\(",..eq.label..," \\qquad ",..adj.rr.label..,"\\)",sep=" ")),
                   formula = y ~ x,
                   eq.with.lhs = "y=",
                   coef.digits = 4,
                   rr.digits = 5,
                   output.type = "latex",
                   parse = FALSE) +
      theme_classic() +
      labs(y = "\\(\\frac{\\textrm{A}_{\\textrm{mix}}}{\\textrm{A}_{Co}}\\)",
           x = "\\(\\frac{\\textrm{A}_{Cr}}{\\textrm{A}_{Co}}\\)") +
      theme(axis.title.y = element_text(angle=0,vjust = 0.5)) 


## tables ----
	wltable <- xtable(Absorbances[
	  ,.( '{$\\lambda$ (\\si{\\nano\\meter})}' = Wavelength,
	      '{Co}' = Co,
	      '{Cr}' = Cr,
	      '{Mixture}' = Mix,
	      '{$\\varepsilon_{\\lambda,Co}$ (\\si{\\per\\Molar\\per\\cm})}' = el_co,
	      '{$\\varepsilon_{\\lambda,Cr}$ (\\si{\\per\\Molar\\per\\cm})}' = el_cr,
	      '{Solver}' = Solver,
	      '{MLRA}' = MLRA
	    
	  )],
	  label = "tbl:wltable",
    caption = "Blank subtracted absorbances of \\CCoStd{} \\ch{Co^{2+}} standard, \\CCrStd{} \\ch{Cr^{3+}}
    standard and a mixture of unknown concentrations at various wavelengths (path length b $=\\SI{1}{\\cm}$).
    Molar absorptivity values calculated from standard absorbances. Predicted mixture absorbance values
    calculated from mixture concentrations determined by analysis method and calculated molar absorptivities",
    digits = 9,
    align =c("l","c",
       "S[table-format=1.3]",
       "S[table-format=1.3]",
       "S[table-format=1.3]|",
       "S[round-precision=3,round-mode=figures,table-auto-round=false,table-format=2.3]",
       "S[round-precision=3,round-mode=figures,table-auto-round=false,table-format=2.3]",
       "|S[table-format=1.3]",
       "S[table-format=1.3]"),
    display = c("d","g","g","g","g","g","g","g","g") 
	  )
@

% value setup
  \newcommand{\CCoStd}{\SI[round-mode=places,round-precision=4]{\Sexpr{C_CO_STD}}{\Molar}}
  \newcommand{\CCrStd}{\SI[round-mode=places,round-precision=4]{\Sexpr{C_CR_STD}}{\Molar}}
  
  \newcommand{\CCoMlr}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(data.frame(summary(glm_conc)$coefficients)["el_co",],
      latexponent(Estimate,sd = Std..Error,fixexp = 0, figs = 3)
    )
    }}{\Molar}}
  \newcommand{\CCrMlr}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(data.frame(summary(glm_conc)$coefficients)["el_cr",],
      latexponent(Estimate,sd = Std..Error,fixexp = 0, figs = 3)
    )
    }}{\Molar}}

  \newcommand{\CCoLin}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(res_mlra["Co"],
     latexponent(Concentration,sd = Err,fixexp = 0, figs = 3))
    }}{\Molar}}
  \newcommand{\CCrLin}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(res_mlra["Cr"],
     latexponent(Concentration,sd = Err,fixexp = 0, figs = 3))
    }}{\Molar}}
  \newcommand{\wllist}{\SIlist[list-units = single]{\Sexpr{
    paste0(Absorbances[,Wavelength],collapse = ";")
    }}{\nano\meter}}
  \newcommand{\CCoWa}{\SI[round-mode=figures,round-precision=3]{0.07552 +- 0.0018}{\Molar}}
  \newcommand{\CCrWa}{\SI[round-mode=figures,round-precision=3]{0.04367 +- 0.0004}{\Molar}}
% values without uncertainty
  \newcommand{\CCoMlrV}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(data.frame(summary(glm_conc)$coefficients)["el_co",],
      latexponent(Estimate,sd = FALSE, fixexp = 0, figs = 3)
    )
    }}{\Molar}}
  \newcommand{\CCrMlrV}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(data.frame(summary(glm_conc)$coefficients)["el_cr",],
      latexponent(Estimate,sd = FALSE, fixexp = 0, figs = 3)
    )
    }}{\Molar}}

  \newcommand{\CCoLinV}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(res_mlra["Co"],
     latexponent(Concentration,sd = FALSE, fixexp = 0, figs = 3))
    }}{\Molar}}
  \newcommand{\CCrLinV}{\SI[round-mode=figures,round-precision=3]{\Sexpr{
    with(res_mlra["Cr"],
     latexponent(Concentration,sd = FALSE, fixexp = 0, figs = 3))
    }}{\Molar}}

%utilities
  \newcommand{\CoandCr}{\ox{2,Co} and \ox{3,Cr}}

\begin{document}
\author{Ariel Lisogorsky}
\title{Simultaneous Spectrophotometric Determination of
		\ch{Co^{2+}} and \ch{Cr^{3+}} Concentrations in Mixture 14}
\date{}
\maketitle

\begin{abstract}
  Absorbances of reference standards and an aqueous mixture of \ox{2,Cobalt} and 
  \ox{3,Chromium} were determined at multiple wavelengths (between 
  \SIlist{\Sexpr{min(Absorbances$Wavelength)};\Sexpr{max(Absorbances$Wavelength)}}{\nano\meter})
  with a Jenway 6302D UV-Vis spectrophotometer. Minimization of \ac{SSR} with the GRG solver 
  provided by Excel,a multiple linear regression and \ac{MLRA}   were evaluated to calculate
  concentrations of \ox{2,Cobalt} and \ox{3,Chromium} in the mixture provided. Both the
  Excel GRG solver and the multiple linear regression methods determined \CCoMlr{} and \CCrMlr{} 
  concentrations(uncertainty determined from multiple linear regression)
  of \CoandCr{} respectively. Calculation by \ac{MLRA} 
  determined \CoandCr{} concentrations to be \CCoLin{} and \CCrLin{}
  respectively in the mixed solution.
\end{abstract}

\acresetall

\section{Data \& Results}

Various methods were used to analyze the data collected in the experiment. In addition to 
established methods in Excel\cite{Harris1998Nonlinear,Segstro2016Methods,Levie1999Estimating} 
the functions in the open source statistics package, R were used to determine the concentrations 
in the \ox{2,Co} and \ox{3,Cr} mixture analyzed. The difference in baseline absorbance between 
two cuvettes was determined at each wavelengths used for analysis. One was used as a blank 
throughout of the experiment while the other was used to contain the analyte. Absorbance 
values recorded for the blank (not shown, range $\pm 0.002$ ) trial in the analyte cuvette were
subtracted from subsequent analyte values recorded within it (\vref{tbl:wltable})

\begin{figure}[!htbp]
  \centering
    <<mlrafig, dev='tikz', external=FALSE, echo=FALSE>>=
    mlraplot
    @
  \caption{\acf{MLRA}\cite{Blanco1989Simple} of a \CoandCr{} mixture by UV-Vis spectrophotometry 
  (at \wllist{}). A least squares linear regression and its equation are shown. Subscripts of 1 and 2
  refer to values of cobalt and chromium respectively}
  \label{fig:mlra}
\end{figure}


\acf{MLRA}\cite{Blanco1989Simple,Segstro2016Methods} was used to simultaneously determine \CoandCr{}
concentrations in the mixture provided(\vref{eq:MLRA,eq:MLRACo,eq:MLRACr,fig:mlra}). A least squares
linear regression and manipulation of values and uncertainty with \cref{eq:MLRACo,eq:MLRACr}
determined concentrations of \CCoLin{} \ox{2,Co} and \CCrLin{} \ox{3,Cr} in the mixed solution. 
Manual and automated computation of slopes and uncertainties in both Excel (\texttt{LINEST()}) and R
(\texttt{lm()}) matched manual calculations\cite{Segstro2016Methods} performed.
 

\begin{figure}[!htbp]
  \centering
  \resizebox{0.8\linewidth}{!}{
    <<wlfig, dev='tikz', external=FALSE, echo=FALSE>>=
    wlplot
    @
    }
  \caption{Absorbance spectra of \CoandCr{} standards and mixture. Predicted absorbance values 
  of mixture with concentrations calculated by approximation models (GRG solver/Multiple linear
  regression and \ac{MLRA}) shown as dashed lines. Note: GRG solver and Multiple linear regression
  have essentially identical values and are therefore shown as one line}
  \label{fig:wl}
\end{figure}

A second calculation method was also employed to determine concentrations of \CoandCr{} in
the mixture analyzed. 
Molar absorptivities ($\varepsilon$) for \CoandCr{} were determined at nine wavelengths (\wllist{})
from absorbances of standard solutions (\vref{tbl:wltable}). Excel solver was then used to determine
\CoandCr{} concentrations that minimized \ac{SSR} (\vref{tbl:jackknife,tbl:wltable}). A similar
calculation (to determine appropriate concentrations as coefficients for molar absorptivities) was
performed with the \texttt{glm()} function in R to determine coefficients for the multi-linear model.
Both methods determined concentrations of  \CCoMlr{} and \CCrMlr{}  for \CoandCr{} respectively\footnote{
Values determined by both methods were not identical but similar to the 5th significant figure.} 
(Uncertainties as reported by R model, calculation of potential uncertainty for the solver method
discussed in \vref{tbl:jackknife}). Expected absorption spectra were determined using calculated molar 
absorptivity coefficients and species concentrations in the mixture For additional clarity, measured
and calculated spectra were plotted (\vref{fig:wl}) to visually assess the accuracy of the model. 
Additionally, to determine specific specific biases in each prediction method, deviation of calculated
values from measured values was graphically plotted(\vref{fig:err})

\begin{figure}[!htbp]
  \centering
  \resizebox{0.8\linewidth}{!}{
    <<errfig, dev='tikz', external=FALSE, echo=FALSE, fig.height = 4, fig.width = 6>>=
    errplot
    @
    }
  \caption{Percent difference in predicted and measured absorption of a \CoandCr mixture
     from two methods of mixture concentration calculation(\vref{fig:wl})}
  \label{fig:err}
\end{figure}

A `jackknife' method\cite{Harris1998Nonlinear} was then used to attempt to estimate the uncertainty 
for the excel solver model. \Vref{tbl:jackknife} shows the effect of removal of individual trials on
the values determined by the solver function. Also shown, are the resulting \acp{SSR} for the 
remaining values as a quantification of their overall impact on the spread of the numbers.
\begin{table}[!hb]
  \centering
  \caption{Results of `jackknife'\cite{Harris1998Nonlinear} method for estimation of error in least 
  squares multi-linear approximation in Excel\cite{Harris1998Nonlinear}}
  \label{tbl:jackknife}
  \sisetup{round-mode=figures, round-precision=3, round-integer-to-decimal=true}
  \begin{tabular}{rS
    S
    S[table-format=1.2e1]
    S[table-format=1.2e1]}
  \toprule
    {Trial Removed} & {\(\left[\ch{Co^{+2}}\right]\)}      & {\(\left[\ch{Cr^{+3}}\right]\)}      & {\(\sum{\left(Abs-Abs_{calc}\right)^2} \)}  \\ \midrule
    460     & 0.07523 & 0.04368 & 5.52E-05 \\
    475     & 0.07553 & 0.04366 & 1.50E-04 \\
    495     & 0.07576 & 0.04363 & 1.34E-04 \\
    510     & 0.07581 & 0.04363 & 1.27E-04 \\
    520     & 0.07558 & 0.04366 & 1.49E-04 \\
    535     & 0.07543 & 0.04362 & 1.21E-04 \\
    550     & 0.07553 & 0.04366 & 1.50E-04 \\
    560     & 0.07540 & 0.04375 & 1.42E-04 \\
    575     & 0.07541 & 0.04373 & 1.47E-04 \\ \midrule
    {None}  & 0.07552 & 0.04367 & 1.50E-04 \\
  $\bar{x}$ & 0.07552 & 0.043669&          \\
  $\pm$     & 0.00018 & 0.00004 &          \\ \bottomrule
  \end{tabular}
\end{table}
\section{Discussion}

It is important to note, that a single data set was collected for the experiment. All of the difference
results reported emerges as a result of differing degrees of emphasis and suceptibility to data variation
in the calculation methods used. 

Purely in terms of mechanic of implementation, the Excel based and Excel specific methods suffer from some common
caveats arising as a result of lack of verbosity, transparency and documentation in calculation.
The excel solver method presented was found to be highly work intensive 
and quite sensitive to user error as a result of the many steps required to `jackknife' the data. In the 
same vein, manual calculation of linear regressions or their error was found to result in similar,hard to
notice mistakes in calculation. However, unless functions are writtenfrom scratch (with significant risk 
of making an error that may go unnoticed) some ambiguity may be present in the methods being used to
calculate certain functions in excel (for example the \texttt{LINEST()} function) resulting in statistical misuse
of data.
Another caveat resulting from poor documentation oof excel functions is the potentially irreproducible behavior
of iterative solving functions like solver which may result in slightly different values given different innitial
guess values. Calculation with solver also requires static change to an excel sheet which means changes
and relationships of data are not explicit from the calculations present in the file (there is no way to 
determine that solver was used to calculate a value in the excel file). This additional effort
and unclear relationship is somewhat mitigated by \citeauthor{Levie1999Estimating} through automation with a macro.
The use of code greatly lowers the risk of error in calculation and increasses repeatability and transparency in 
calculation. The \ac{MLRA} method may be implemented in excel purely through cell references and can therefore 
function dynamically with data when implemented making all calculations explicitly available for review. Numerically,
the values determined with a dedicated data analysis package (R) were found to be essentially equivalent to those determined in excel. The only numerical difference was between the values determined by solver in the
concentration and uncertainty values determined by the implementation of the multiple linear regression in excel
to the coefficients determined by the multiple linear regression provided by R.

The concentrations determined by all of the methods were largely similar (genereally within one standard deviation
of each other) as a result of the relatively low variability in data. Both methods used however, had different leverage points which exerted some influence in overall results. For the multple linear regression (Solver) method, the
set of measurements at 460 \si{\nano\meter} had the largest influence on error values (\vref{tbl:jackknife}) and
had significant offset. As an edge case where there is less overlap in absorption spectra, the measurement
is more prone to error\cite{Segstro2016Methods,Blanco1989Simple}. Due to it's significant distance from the other
points present, at the optimum concentrations determined, it is a large source of the measured error and therefore
exerting significant leverage on the other points. In the \ac{MLRA} calculations, the sets of measurement at the
 two longest wavelengths, (corresponding the greatest difference in absorptivity between \CoandCr{}) are  
 significantly distant from the rest of tha data points increasing their potential for leverage. While there is no
 clear evidence that the points are significantly causing skew in this scenario (their removal did not significantly
 affect calculated concentrations) as a method for analysis, it is significantly more sensitive to outliers edge
 regions where it is expected that accuracy is lower.

 Overall, three different pairs of concentrations were determined by the calculations methods used. The Excel
 Solver method to aproximate a multiple linear regression and the R multiple regression function both determined
 extremely similar values (Below the threshold for apropriate significant significant figures.).However, the 
 different methods used to calculate uncertainties in coefficients resulted 
 in calculated uncertainties that are substantially different from each other. The jackknife method predicted
 lower uncertainties than those reported by the R function. The calculations for \ac{MLRA} 
 resulted in different values that were approximately one standard deviation away from those calculated by the 
 multiple linear regression and therefore within a 95\% confidence interval. As a calculation method, \ac{MLRA}
 was significantly simpler to implement in Excel and potential leverage and influence points were easier to identify
 though the relationship to the data was more distant. Lastly it is important to re-affirm that the data presented
 only represents a single replicate of the method and is therefore extremely vulnerable to method error that might
 present precise data with low variability but poor accuracy without much assurance.  
 
\section{Appendix}

\subsection{Tables}
<<tables, results='asis'>>=
  print(wltable)
@
\FloatBarrier
\subsection{Calculations}

\begin{align}
  \frac{\textrm{A}_{\textrm{m}}}{\textrm{A}_{\textrm{s1}}}=&
  \frac{c_{\textrm{m,1}}}{c_{\textrm{s1}}}
  \frac{\textrm{A}_{\textrm{s2}}}{\textrm{A}_{\textrm{s1}}} \times
  \frac{c_{\textrm{m,2}}}{c_{\textrm{s2}}} \label{eq:MLRA} \\
   y=& b + x \times m \nonumber \\
   c_{\textrm{m,1}}=&b\times c_{\textrm{s1}}&
   \num[round-precision=4]{\Sexpr{coef(lm_mlra)[1]}}\times \CCoStd{}=& \CCoLinV{} \ \ch{Co^{+2}} %
   \label{eq:MLRACo}\\
   c_{\textrm{m,2}}=&m\times c_{\textrm{s2}}&
     \num[round-precision=4]{\Sexpr{coef(lm_mlra)[2]}}\times \CCrStd{}=& \CCrLinV{} \ \ch{Cr^{+3}} %
   \label{eq:MLRACr}
\end{align}

\bibliography{Remote}
\end{document}