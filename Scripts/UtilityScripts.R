

# LaTeX Formatting fns
latexponent <-
  function(n,
           figs = getOption('digits'),
           sd = F,
           fixexp = mag,
           outype = 'siunit', calculate = F) {
    
    if (calculate) {
      if (sd) {
        sd <- sd(n)
      }
      
      n <- mean(n)
    }
    
    mag <- min(floor(log10(abs(n))))
    figs <- figs - 1
    digs <- n / 10 ^ fixexp
    
    roundfigs <- max(c(figs - (mag - fixexp),
                       0))
    outstr <- switch(outype,
                     latex={c(open="$",mid=" \\times 10^{", close="}$")},
                     siunit={c(open="",mid="e", close="")})
    sd <- if (sd) {
      paste0("(",
             formatC(
               sd * 10 ^ (figs - mag),
               digits = 0,
               drop0trailing = F,
               format = 'f'
             ),
             ")") } else {
      "" }
    
    if (fixexp == 0){
      paste0( formatC(
          digs,
          digits = roundfigs,
          drop0trailing = F,
          format = 'f'
        ),
        sd)
      }else{
    paste0(
      outstr['open'],
      formatC(
        digs,
        digits = roundfigs,
        drop0trailing = F,
        format = 'f'
      ),
      sd,
      outstr['mid'],
      fixexp,
      outstr['close']
    )
      }
}

qelim <- function (x, p=0.05,maxrem = min(2, length(x)-1)){
  
  if (!require("pacman")) install.packages("pacman")
  pacman::p_load("outlier")
  
  qtest <-dixon.test(x)
  
  num <- as.numeric(strsplit(qtest$alternative, " ")[[1]][3])
  
  if (qtest$p.value < p) {
    message(
      paste("Eliminating value of:",num,
        "with p value of:",qtest$p.value)
      )
    y<-rm.outlier(x)

    if (maxrem > 1) {qelim(y,maxrem-1,p)} else {y}
  
  } else {
    message(
      paste("Not eliminating value of:",num,
        "with p value of:",qtest$p.value))
    x
  }
}

Keep_First_Unique <- function(vec) 
{
  vals<-c("")
  out<-vector()
  for(i in vec) {
    if(i %in% vals) {
      out<-c(out,NA)
    } else {
      out<-c(out,i)
      vals<-c(vals,i)
    }
  }
  out
  
}
